import asyncio
import json

import aiosqlite
from sanic import Sanic, response, websocket

DATABASE_PATH = "../database/db.sqlite3"
RECV_WAIT = 0.25
app = Sanic(__name__)

app.static("/public", "./public")


@app.route("/")
async def index(request):
    with open("./public/index.html") as file:
        contents = file.read()
    return response.html(contents)


@app.websocket("/messages")
async def messages(request, ws):
    last_id = 0

    while True:
        new_messages = await get_messages(last_id)
        if new_messages:
            last_id = new_messages[-1]["id"]
            await ws.send(json.dumps([dict(msg) for msg in new_messages]))

        message = await recv_message(ws, RECV_WAIT)

        if message:
            await add_message(message["user"], message["content"])

    print("Connection closed")


async def add_message(user: str, message: str):
    async with aiosqlite.connect(DATABASE_PATH) as conn:
        await conn.execute(
            "insert into messages (user, message) values (:user, :message)",
            {"user": user, "message": message},
        )
        await conn.commit()


async def get_messages(last_message_id: int):
    async with aiosqlite.connect(DATABASE_PATH) as conn:
        conn.row_factory = aiosqlite.Row
        async with conn.execute(
            "select * from messages where id > :id order by id limit 10",
            {"id": last_message_id},
        ) as cursor:
            return await cursor.fetchall()


async def recv_message(ws, timeout: float):
    try:
        message = await asyncio.wait_for(ws.recv(), timeout=timeout)
        return json.loads(message)
    except asyncio.TimeoutError:
        return None


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True, protocol=websocket.WebSocketProtocol)
