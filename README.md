# WebSocket Chat

This project creates a simple client / server chat room using web sockets.

## Technologies Used

### Server Side

- Python 3.6+
- Sanic
- SQLite

### Client Side

- React
- JSS
- Parcel

## Setting up the Application

### Prerequisites

There are a few things you'll need to have installed to get this project
running:

- npm
- python 3.6
- sqlite3

### Setting up the Database

To set up the database you'll need to create a new sqlite database and load the
schema. The server is hard coded to look for the database file in a specific
place. To set up the database run the following command from the root of the
project:

    sqlite3 database/db.sqlite3 ".read database/schema.sql"

### Installing Client Dependencies and Building

The client side code is compiled with Parcel, to build it run:

    cd client
    npm install
    npm run build

### Installing Server Dependencies and Running

    cd server
    pip install -r requirements.txt
    python server.py

## Interacting with the Application

Open your browser to `http://127.0.0.1:8000`. You can set your handle and type
messages in the input box and they will show up in the messages list. You can
load up multiple tabs with the chat program and chat between the two tabs.

## Things That Could be Better

There are a few things hard coded into the application, such as the location
of the database file, that would be better as environment
variables or something else.
