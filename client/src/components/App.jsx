import React from 'react';
import { createUseStyles } from 'react-jss';
import Messages from './Messages.jsx';
import Input from './Input.jsx';

const messagesURI = `ws://${document.location.host}/messages`;

const useStyles = createUseStyles({
  '@global': {
    '*': {
      boxSizing: 'border-box',
      fontFamily: 'Helvetica, sans-serif',
    },
  },
  app: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    backgroundColor: '#ddd',
  },
  content: {
    margin: '25px auto',
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'column',
    height: 'calc(100% - 50px)',
    width: '600px',
  },
});

const App = () => {
  const classes = useStyles();
  const [webSocket, setWebSocket] = React.useState();

  React.useEffect(() => {
    const webSocket = new WebSocket(messagesURI);
    setWebSocket(webSocket);

    return () => {
      webSocket.close();
      setWebSocket(null);
    };
  }, [setWebSocket]);

  return (
    <div className={classes.app}>
      <div className={classes.content}>
        <Messages webSocket={webSocket} />
        <Input webSocket={webSocket} />
      </div>
    </div>
  );
};

export default App;
