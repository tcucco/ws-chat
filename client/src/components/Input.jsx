import React from 'react';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  container: {
    padding: '5px',
    borderTop: '5px solid #ddd',
    display: 'flex',
    flexDirection: 'row',
  },
  input: {
    border: 'none',
    outline: 'none',
    flex: 1,
  },
  handle: {
    border: 'none',
    outline: 'none',
    width: '100px',
  }
});

const Input = ({ webSocket }) => {
  const classes = useStyles();

  const [message, setMessage] = React.useState('');
  const [handle, setHandle] = React.useState('user' + (Math.random() * 1000).toFixed());

  const onChangeMessage = React.useCallback(event => setMessage(event.target.value));
  const onChangeHandle = React.useCallback(event => setHandle(event.target.value));

  const onSubmit = React.useCallback(event => {
    event.preventDefault();

    const newMessage = {
      user: handle,
      content: message,
    };

    webSocket.send(JSON.stringify(newMessage));
    setMessage('');
  }, [handle, message]);

  return (
    <form onSubmit={onSubmit}>
      <div className={classes.container}>
        <input className={classes.input} type="text" value={message} onChange={onChangeMessage} />
        &nbsp;as&nbsp;
        <input className={classes.handle} type="text" value={handle} onChange={onChangeHandle} />
        <input type="submit" value="Send" />
      </div>
    </form>
  );
};

export default Input;
