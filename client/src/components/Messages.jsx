import React from 'react';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  container: {
    flex: 1,
  },
});

const Messages = ({ webSocket }) => {
  const classes = useStyles();
  const [messages, setMessages] = React.useState([]);
  const handleMessages = React.useCallback(
    event => {
      const newMessages = JSON.parse(event.data);
      setMessages([...messages, ...newMessages]);
    },
    [messages, setMessages],
  );

  React.useEffect(() => {
    if (webSocket) {
      webSocket.addEventListener('message', handleMessages);
    }

    return () => {
      if (webSocket) {
        webSocket.removeEventListener('message', handleMessages);
      }
    };
  }, [webSocket, handleMessages]);

  return (
    <div className={classes.container}>
      {messages.map(msg => (
        <div key={msg.id}>
          {msg.created_at} - {msg.user} - {msg.message}
        </div>
      ))}
    </div>
  );
};

export default Messages;
