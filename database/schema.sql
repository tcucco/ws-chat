create table messages (
  id integer not null primary key,
  user text not null,
  message text not null,
  created_at datetime not null default current_timestamp
);
